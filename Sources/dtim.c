//***********************************************************************************************************************************
// FILE: dtim.c
//
// DECRIPTION:
// Contains functions for initializing DMA timers 0, 1, 2, and 3 and implementing a busy delay.
//
// AUTHORS:
// Kevin R. Burger (burgerk@asu.edu)
// Computer Science & Engineering
// Arizona State University
// Tempe, AZ 85287-8809
// http://www.devlang.com
//***********************************************************************************************************************************
#include "dtim.h"
#include "gpio.h"
#include "int.h"

//===================================================================================================================================
// Private Preprocessor Macros
//===================================================================================================================================

// The base address in the peripheral register range of the DTIM module registers.
#define DTIM_BASE  0x40000400

// The addresses of the DTIM registers for timers n = 0, 1, 2, and 3.
#define DTIM_DTCN(timer)   *(volatile uint32 *) (DTIM_BASE + 0x0C + ((timer) << 6))
#define DTIM_DTCR(timer)   *(volatile uint32 *) (DTIM_BASE + 0x08 + ((timer) << 6))
#define DTIM_DTER(timer)   *(volatile uint8  *) (DTIM_BASE + 0x03 + ((timer) << 6))
#define DTIM_DTMR(timer)   *(volatile uint16 *) (DTIM_BASE + 0x00 + ((timer) << 6))
#define DTIM_DTRR(timer)   *(volatile uint32 *) (DTIM_BASE + 0x04 + ((timer) << 6))
#define DTIM_DTXMR(timer)  *(volatile uint8  *) (DTIM_BASE + 0x02 + ((timer) << 6))

//===================================================================================================================================
// Static Function Declarations
//===================================================================================================================================

// ISR for DMA timer 0.
__declspec(interrupt) static void dtim0_isr();

// ISR for DMA timer 1.
__declspec(interrupt) static void dtim1_isr();

// ISR for DMA timer 2.
__declspec(interrupt) static void dtim2_isr();

// ISR for DMA timer 3.
__declspec(interrupt) static void dtim3_isr();

//===================================================================================================================================
// Static Global Variable Definitions
//===================================================================================================================================

// Array of function pointers to user callbacks for timers 0-4.
static int_isr g_callbacks[4] = { 0 };

// Array of function pointers to ISR's for timers 0-4.
static int_isr g_isrs[4] = {
    dtim0_isr,
    dtim1_isr,
    dtim2_isr,
    dtim3_isr
};

//===================================================================================================================================
// Function Definitions
//===================================================================================================================================

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_busy_delay_ms()
//
// DESCRIPTION
// Implements a busy delay of p_msecs milliseconds.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_busy_delay_ms(dtim p_timer, int p_msecs)
{
    dtim_busy_delay_us(p_timer, 1000 * p_msecs);
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_busy_delay_us()
//
// DESCRIPTION
// Implements a busy delay of p_usecs microseconds.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_busy_delay_us(dtim p_timer, int p_usecs)
{
    // DTCN - DMA Timer Counter Register
    //
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 31:0  CNT = 0x00000000   Timer counter. Reset timer counter to 0.
    MCF_DTIM_DTCN(p_timer) = 0x00000000;

    // DTRR - DMA Timer Reference Register
    //
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 31:0  REF = p_usecs - 1  Reference value. The reference value in DTRR is matched after DTRR+1 timer
    //                          intervals. Therefore, the value written into this register must be p_usecs - 1.
    DTIM_DTRR(p_timer) = (uint32)(p_usecs - 1);

    // DTER - DMA Timer Event Register
    //
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 7:2   Unused = 000000
    //   1   REF    = 1         Output reference. 1 = clear any pending DTIM interrupt requests.
    //   0   CAP    = x         N/A.
    //
    // DTER = 000000 1 0 = 0000 0010 = 0x02
    DTIM_DTER(p_timer) |= 0x02;

    // Enable the timer by writing 1 to bit 0 (RST) of the DTMR register.
    DTIM_DTMR(p_timer) |= 0x0001;

    // Bit 1 (REF) of DTER will go to 1 when the timer counter in DTCN reaches the reference count in DTRR. Wait
    // in a busy loop until that happens.
    while (~DTIM_DTER(p_timer) & 0x02) {}
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_disable()
//
// DESCRIPTION
// Disables DMA timer p_timer.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_disable(dtim p_timer)
{
    // Disable the timer by clearing bit 0 (RST).
    DTIM_DTMR(p_timer) &= 0xFE;
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_init()
//
// DESCRIPTION
// Initializes DMA timer p_timer so the clock rate is 80 MHz. This causes the counter to increment once per microsecond.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_init(dtim p_timer)
{
    // DTMR - DMA Timer MainMode Register
    //
    // Bits  Field  Description
    // ----  -----  --------------------------------------------------------------------------------------------
    // 15:8  PS     Prescaler
    //  7:6  CE     Capture Edge
    //    5  OM     Output MainMode
    //    4  ORRI   Output Reference Request, Interrupt enable
    //    3  FRR    Free Run/Restart
    //  2:1  CLK    Input Clock Source
    //    0  RST    Reset Timer
    //
    // We first reset the timer by setting bit 0 (RST) and then clearing it.
    DTIM_DTMR(p_timer) |= 0x0001;
    DTIM_DTMR(p_timer) &= 0xFFFE;

    // DTMR - DMA Timer MainMode Register
    //
    // Field Value               Description
    // ------------------------  -------------------------------------------------------------------------------
    // PS   = 01001111 (79 dec)  Divide the clock input (see CLK) by 80. f_sys is 80 MHz, so this configs the
    //                           clock for the DTIM to be 1 Mhz.
    // CE   = 00                 Disable capture event output.
    // OM   = x                  N/A since the timer is not configured for output mode.
    // ORRI = 0                  Disable DMA or interrupt requests.
    // FRR  = 1                  Halt when counter matches reference.
    // CLK  = 01                 Input clock source is f_sys / 1 (80 MHz; see PS).
    // RST  = 0                  Reset timer; After configuring other regs we will enable the timer by setting
    //                           RST = 1.
    //
    // DTMR = 01001111 00 0 0 1 01 0 = 0100 1111 0000 1010 = 0x4F0A
    //
    // Note: Since CLK is 01, the DMA module is clocked by f_sys / 1, i.e., the DMA module clock frequency will
    // be 80 MHz. With PS set to 79, the DMA timer's clock frequency will be 80 MHz / 80 = 1 MHz. This means
    // that the DMA timer clock period will be .0000001 sec = 1 us.
    DTIM_DTMR(p_timer) = 0x4F0A;

    // DTXMR - DMA Timer Extended MainMode Register
    ///
    // Bits  Field           Description
    // ----  ------          -----------------------------------------------------------------------------------
    //   7   DMAEN  = 0      DMA request disabled.
    //   6   HALTED = 1      Timer stops counting when the ColdFire core is halted.
    // 5:1   Unused = 00000  Should be cleared.
    //   0   MODE16 = 0      Increment timer counter by 1.
    //
    // DTXMR = 0 1 00000 0 = 0100 0000 = 0x40
    DTIM_DTXMR(p_timer) = 0x40;
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_interrupt()
// 
// DESCRIPTION
// Configures DMA timer p_timer to generate periodic interrupts with period p_usecs microseconds. If requested, the user's callback
// function p_callback will be called.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_interrupt(dtim p_timer, int p_usecs, int_isr p_callback)
{
    // DTMR - DMA Timer MainMode Register
    //
    // Bits  Field  Description
    // ----  -----  --------------------------------------------------------------------------------------------
    // 15:8  PS     Prescaler
    //  7:6  CE     Capture Edge
    //    5  OM     Output MainMode
    //    4  ORRI   Output Reference Request, Interrupt enable
    //    3  FRR    Free Run/Restart
    //  2:1  CLK    Input Clock Source
    //    0  RST    Reset Timer
    //
    // We first reset the timer by setting bit 0 (RST) and then clearing it.
    DTIM_DTMR(p_timer) |= 0x0001;
    DTIM_DTMR(p_timer) &= 0xFFFE;

    // DTMR - DMA Timer MainMode Register
    //
    // Field Value               Description
    // ------------------------  -------------------------------------------------------------------------------
    // PS   = 01001111 (79 dec)  Divide the clock input (see CLK) by 80. f_sys is 80 MHz, so this configs the
    //                           clock for the DTIM to be 1 Mhz.
    // CE   = 00                 Disable capture event output.
    // OM   = x                  N/A since the timer is not configured for output mode.
    // ORRI = 1                  Enable interrupt requests.
    // FRR  = 1                  Timer count is reset immediately upon reaching reference count.
    // CLK  = 01                 Input clock source is f_sys / 1 (80 MHz; see PS).
    // RST  = 0                  Reset timer; After configuring other regs we will enable the timer by setting
    //                           RST = 1.
    //
    // DTMR = 01001111 00 0 1 1 01 0 = 0100 1111 0001 1010 = 0x4F1A
    ///
    // Note: Since CLK is 01, the DMA module is clocked by f_sys / 1, i.e., the DMA module clock frequency will
    // be 80 MHz. With PS set to 79, the DMA timer's clock frequency will be 80 MHz / 80 = 1 MHz. This means
    // that the DMA timer clock period will be .0000001 sec = 1 us.
    DTIM_DTMR(p_timer) = 0x4F1A;

    // DTXMR - DMA Timer Extended MainMode Register
    //
    // Bits  Field           Description
    // ----  ------          -----------------------------------------------------------------------------------
    //   7   DMAEN  = 0      DMA request disabled.
    //   6   HALTED = 1      Timer stops counting when the ColdFire core is halted.
    // 5:1   Unused = 00000  Should be cleared.
    //   0   MODE16 = 0      Increment timer counter by 1.
    //
    // DTXMR = 0 1 00000 0 = 0100 0000 = 0x40
    DTIM_DTXMR(p_timer) = 0x40;

    // DTCN - DMA Timer Counter Register
    //
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 31:0  CNT = 0x00000000   Timer counter. Reset timer counter to 0.
    MCF_DTIM_DTCN(p_timer) = 0x00000000;

    // DTRR - DMA Timer Reference Register
    //
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 31:0  REF = p_usecs - 1  Reference value. The reference value in DTRR is matched after DTRR+1 timer
    //                          intervals. Therefore, the value written into this register must be p_usecs - 1.
    DTIM_DTRR(p_timer) = (uint32)(p_usecs - 1);

    // DTER - DMA Timer Event Register
    ///
    // Bits  Field              Description
    // ----  -----------------  --------------------------------------------------------------------------------
    // 7:2   Unused = 000000
    //   1   REF    = 1         Output reference. 1 = clear any pending DTIM interrupt requests.
    //   0   CAP    = x         N/A.
    //
    // DTER = 000000 1 0 = 0000 0010 = 0x02
    DTIM_DTER(p_timer) |= 0x02;

    // Configure interrupt handling for the DMA timer.
    g_callbacks[p_timer] = p_callback;
    int_config(DTIM_INT_SRC(p_timer), DTIM_INT_LVL(p_timer), DTIM_INT_PRI(p_timer), g_isrs[p_timer]);

    // Enable the timer by writing 1 to bit 0 (RST) of the DTMR register.
    DTIM_DTMR(p_timer) |= 0x0001;
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_stopw_start()
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(register_abi) asm void dtim_stopw_start()
{
    movea.l    #0x400004C0, a0         /* a0 <- addr of DTMR3 */
    move.w     #0x0001, (a0)           /* DTMR3 <- 0x0001 */
    move.w     #0x0000, (a0)           /* DTMR3 <- 0x0000 */
    movea.l    #0x400004CC, a1         /* a1 <- addr of DTCN3 */
    move.l     #0x00000000, (a1)       /* DTCN3 <- 0x00000000 */
    move.w     #0x0003, (a0)           /* DTMR <- 0x0003 */
    rts
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_stopw_stop()
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(register_abi) asm uint32 dtim_stopw_stop()
{
    move.l     0x400004CC, d0          /* do <- addr of DTCN3 */
    rts
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim0_isr()
// 
// DESCRIPTION
// Called when the counter of DMA timer 0 matches the reference count. Clear the interrupt request flag and call the user's callback
// function.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(interrupt) static void dtim0_isr()
{
    DTIM_DTER(0) |= 0x02;
    if (g_callbacks[0]) g_callbacks[0]();
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim1_isr()
// 
// DESCRIPTION
// Called when the counter of DMA timer 1 matches the reference count. Clear the interrupt request flag and call the user's callback
// function.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(interrupt) static void dtim1_isr()
{
    DTIM_DTER(1) |= 0x02;
    if (g_callbacks[1]) g_callbacks[1]();
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim2_isr()
// 
// DESCRIPTION
// Called when the counter of DMA timer 2 matches the reference count. Clear the interrupt request flag and call the user's callback
// function.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(interrupt) static void dtim2_isr()
{
    DTIM_DTER(2) |= 0x02;
    if (g_callbacks[2]) g_callbacks[2]();
}

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim3_isr()
// 
// DESCRIPTION
// Called when the counter of DMA timer 3 matches the reference count. Clear the interrupt request flag and call the user's callback
// function.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(interrupt) static void dtim3_isr()
{
    DTIM_DTER(3) |= 0x02;
    if (g_callbacks[3]) g_callbacks[3]();
}
