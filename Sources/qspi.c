//**************************************************************************************************************
// FILE: qspi.c
//
// DECRIPTION
// functions to use qspi protocol
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "qspi.h"

#define QSPI_TX_RAM_BASE  0x00
#define QSPI_RX_RAM_BASE  0x10
#define QSPI_CMD_RAM_BASE  0x20

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void qspi_init(int bits, int freq, int cpol, int cpha)
//
// DESCRIPTION
//  inits qspi module
//--------------------------------------------------------------------------------------------------------------
void qspi_init(int bits, int freq, int cpol, int cpha)
{
	gpio_port_init(13,0,1,1,1);
	gpio_port_init(13,2,1,1,1);
	
	MCF_QSPI_QDLYR &= 0; //clear contents
	
	MCF_QSPI_QMR = 0;
	MCF_QSPI_QMR |=0x8000; //sets mstr
	MCF_QSPI_QMR |=  ((bits % 16) << 10); // set bits
	MCF_QSPI_QMR |=  (cpol << 9); //set polarity
	MCF_QSPI_QMR |=  (cpha << 8); //set phase
	MCF_QSPI_QMR |=  (80000000/(2*freq)); //set baud
	
	MCF_QSPI_QWR = 0;
}


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void qspi_tx(int n, uint8 data[])
//
// DESCRIPTION
//  transmits data[] over spi
//--------------------------------------------------------------------------------------------------------------
void qspi_tx(int n, uint8 data[])
{
	MCF_QSPI_QAR = QSPI_TX_RAM_BASE;
	
	for(int i = 0; i < n; i++)
	{
		MCF_QSPI_QDR = data[i];
	}
	
	MCF_QSPI_QAR = QSPI_CMD_RAM_BASE;
	
	for(int i = 0; i < n; i++)
	{
		MCF_QSPI_QDR = 0x4000;
	}
	
	MCF_QSPI_QWR &= 0xFFF0; //start and end address
	MCF_QSPI_QWR &= 0xF0FF;
	MCF_QSPI_QWR |= ((n-1) << 8);
	MCF_QSPI_QIR = 1;
	MCF_QSPI_QDLYR |= 0x8000;
	
	while(!(MCF_QSPI_QIR & 1))
	{
			
	}

	MCF_QSPI_QIR = 1;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void qspi_rx(int n, uint8 data[])
//
// DESCRIPTION
// receives data
//--------------------------------------------------------------------------------------------------------------
void qspi_rx(int n, uint8 data[])
{
	MCF_QSPI_QAR = QSPI_RX_RAM_BASE;

	for(int i = 0; i < n; i++)
	{
		data[i] = (uint8) MCF_QSPI_QDR;
	}

}