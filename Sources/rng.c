//**************************************************************************************************************
// FILE: rng.c
//
// DECRIPTION
// functions to use random number generator
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************
#include "rng.h"

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void rng_init()
//
// DESCRIPTION
// start up random generator
//--------------------------------------------------------------------------------------------------------------
void rng_init()
{
	MCF_RNGA_RNGCR = 0;
	MCF_RNGA_RNGCR |= 1;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: uint32 rng_rand()
//
// DESCRIPTION
// retrieves random number
//--------------------------------------------------------------------------------------------------------------
uint32 rng_rand()
{
	while(((MCF_RNGA_RNGSR >> 8) & 1) ==0)
	{}
		return MCF_RNGA_RNGOUT;
}