//**************************************************************************************************************
// FILE: ledoff.c
//
// DECRIPTION
// contains functions for led off game
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "ledoff.h"

uint8 win = 0xFF;

uint8 g_x;
uint8 g_y;

uint8 patternr[16];
uint8 patterng[16];
uint8 patternb[16];

uint8 patternrc[16];
uint8 patterngc[16];
uint8 patternbc[16];

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pntr_reset()
//
// DESCRIPTION
// resets pntr position
//--------------------------------------------------------------------------------------------------------------
void pntr_reset()
{
	g_x = 4;
	g_y = 4;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void set_test()
//
// DESCRIPTION
// sets matrix to test config
//--------------------------------------------------------------------------------------------------------------
void set_test()
{
	patternr[7] = 0b01000010;
	patternr[6] = 0b11100111;
	patternr[5] = 0b01000010;
	patternr[4] = 0b00000000;
	patternr[3] = 0b00000000;
	patternr[2] = 0b01000010;
	patternr[1] = 0b11100111;
	patternr[0] = 0b01000010;
		
	patterng[7] = 0b01000010;
	patterng[6] = 0b11100111;
	patterng[5] = 0b01000010;
	patterng[4] = 0b00000000;
	patterng[3] = 0b00000000;
	patterng[2] = 0b01000010;
	patterng[1] = 0b11100111;
	patterng[0] = 0b01000010;
	pattern_combine();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void set_rand()
//
// DESCRIPTION
// sets matrix to rand config
//--------------------------------------------------------------------------------------------------------------
void set_rand()
{
	uint8 loop = (((uint8)rng_rand())%13) + 13;
	pattern_reset_all();
	
	uint8 x[25];
	uint8 y[25];
	
	toggle(((uint8)rng_rand())%8, ((uint8)rng_rand())%8);
	
	for(int i = 0; i < loop; i++)
	{
		
		x[i] = ((uint8)rng_rand())%8;
		y[i] = ((uint8)rng_rand())%8;
		
		while((patternr[x[i]] >> y[i])&1)
		{
			x[i] = ((uint8)rng_rand())%8;
			y[i] = ((uint8)rng_rand())%8;
		}
		toggle(x[i], y[i]);
		
	}
	pattern_combine();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void set_win()
//
// DESCRIPTION
// sets win config
//--------------------------------------------------------------------------------------------------------------
void set_win()
{
	pattern_reset(patternrc);
	pattern_reset(patterngc);
	pattern_reset(patternbc);
	patterngc[7] = 0b01111110;
	patterngc[6] = 0b10000001;
	patterngc[5] = 0b10101001;
	patterngc[4] = 0b10000101;
	patterngc[3] = 0b10000101;
	patterngc[2] = 0b10101001;
	patterngc[1] = 0b10000001;
	patterngc[0] = 0b01111110;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void button_c()
//
// DESCRIPTION
// button c was pushed
//--------------------------------------------------------------------------------------------------------------
void button_c()
{

	pntr_reset();
	set_rand();
}
 
//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void button_z()
//
// DESCRIPTION
// button z was pushed
//--------------------------------------------------------------------------------------------------------------
void button_z()
{
	if(win)
	{
		toggle_adj();
		pattern_combine();
	}
	else
	{
		pntr_reset();
		set_rand();
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pntr_up()
//
// DESCRIPTION
// moves pntr up
//--------------------------------------------------------------------------------------------------------------
void pntr_up()
{
	if(win)
	{
	if(g_y == 7) g_y = 0;
	else g_y = (g_y + 1)%8;
	pattern_combine();
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pntr_down()
//
// DESCRIPTION
// moves pntr down
//--------------------------------------------------------------------------------------------------------------
void pntr_down()
{
	if(win)
		{
	if(g_y == 0) g_y = 7;
	else g_y = (g_y - 1) %8;
	pattern_combine();
		}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pntr_right()
//
// DESCRIPTION
// moves pointer right
//--------------------------------------------------------------------------------------------------------------
void pntr_right()
{
	if(win)
	{
	if(g_x == 0) g_x = 7;
	else g_x = (g_x - 1)%8;
	pattern_combine();
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pntr_left()
//
// DESCRIPTION
// moves pntr left
//--------------------------------------------------------------------------------------------------------------
void pntr_left()
{
	if(win)
	{
	if(g_x ==7) g_x = 0;
	else g_x = (g_x + 1)%8;
	pattern_combine();
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pushb1()
//
// DESCRIPTION
// pushb1 was pressed
//--------------------------------------------------------------------------------------------------------------
void pushb1()
{
pntr_reset();	
set_test();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pushb2()
//
// DESCRIPTION
// unused
//--------------------------------------------------------------------------------------------------------------
void pushb2()
{}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pattern_reset(uint8 pattern[16])
//
// DESCRIPTION
// resets pattern to all 0
//--------------------------------------------------------------------------------------------------------------
void pattern_reset(uint8 pattern[16])
{
	for(int i = 0; i< 16; i++)
	{
		pattern[i] = 0;
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pattern_reset_all()
//
// DESCRIPTION
// resets all pattern arrays
//--------------------------------------------------------------------------------------------------------------
void pattern_reset_all()
{
	pattern_reset(patternr);
	pattern_reset(patterng);
	pattern_reset(patternb);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pattern_combine())
//
// DESCRIPTION
// copies pattern arrays, modifies to include pntr and then updates display
//--------------------------------------------------------------------------------------------------------------
void pattern_combine()
{
	win = 0;
	
	for(int i = 0; i<8; i++)
	{
		win |= patternr[i];	
	}
	
	if(win == 0)
		{
		set_win();
		oct_ledm_display_pattern(patternrc, patterngc, patternbc);
		}
	
	else
	{
		for(int i = 0; i < 16; i++)
		{
			if(i > 7)
			{
				patternr[i] = 0;
				patterng[i] = 0;
				patternb[i] = 0;
			}
			patternrc[i] = patternr[i];
			patterngc[i] = patterng[i];
			patternbc[i] = patternb[i];	
		}
	
		patternbc[g_x] |= (uint8)(1 << g_y);
	
		if((patternrc[g_x] >> g_y)&1 ==1)
		{
			patternrc[g_x] &= ~(1 << g_y);
			patternbc[g_x] &= ~(1 << g_y);
		}
	
		oct_ledm_display_pattern(patternrc, patterngc, patternbc);
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void toggle_adj()
//
// DESCRIPTION
// toggles adjacent led on button push
//--------------------------------------------------------------------------------------------------------------
void toggle_adj()
{
	toggle(g_x, g_y);
	toggle(g_x-1, g_y);
	toggle(g_x+1, g_y);
	toggle(g_x, g_y+1);
	toggle(g_x, g_y-1);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void toggle(uint8 x, uint8 y)
//
// DESCRIPTION
// toggles led with passed coordinates
//--------------------------------------------------------------------------------------------------------------
void toggle(uint8 x, uint8 y)
{
		uint8 temp_red =(uint8)((~(patternr[x] >> y))&1) << y;
		uint8 temp_green =(uint8)((~(patterng[x] >> y))&1) << y;
		patternr[x] &= ~(1 << y);
		patternr[x] |= temp_red;
		patterng[x] &= ~(1 << y);
		patterng[x] |= temp_green;
}