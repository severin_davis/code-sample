//**************************************************************************************************************
// FILE: i2c.c
//
// DECRIPTION
// functions to use i2c protocol
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "i2c.h"

#define I2C_READ  1
#define I2C_WRITE 0

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_acquire_bus (i2c_mod n)
//
// DESCRIPTION
// waits to acquire i2c bus
//--------------------------------------------------------------------------------------------------------------
void i2c_acquire_bus (i2c_mod n)
{
	while((MCF_I2C_I2SR(n)& 0x20))
	{}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_init(i2c_mod n, int freq, uint8 addr)
//
// DESCRIPTION
// inits i2c module
//--------------------------------------------------------------------------------------------------------------
void i2c_init(i2c_mod n, int freq, uint8 addr)
{
	if(n == i2c_mod_0)
	{
		gpio_port_init(0xB, 0, 1, 1, 0); 
		gpio_port_init(0xB, 1, 1, 1, 0); 	
	}
	else
	{
		gpio_port_init(0x12, 0, 2, 1, 0); 
		gpio_port_init(0x12, 1, 2, 1, 0); 	
	}
	
	MCF_I2C_I2ADR(n) = (uint8) (addr << 1);
	
	MCF_I2C_I2FDR(n) = 0x3A;
	
	freq=freq;
	
	i2c_reset(n);
	
	if(MCF_I2C_I2SR(n) & 0x20)
	{
		MCF_I2C_I2CR(n) = 0x00;
		MCF_I2C_I2CR(n) = 0xA0;
		uint8 dummy = MCF_I2C_I2DR(n);
		MCF_I2C_I2SR(n) = 0x00;
		MCF_I2C_I2CR(n) = 0x00;
		MCF_I2C_I2CR(n) = 0x80;
	}
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_reset(i2c_mod n)
//
// DESCRIPTION
// resets i2c module
//--------------------------------------------------------------------------------------------------------------
void i2c_reset(i2c_mod n)
{
	MCF_I2C_I2CR(n) = 0x80;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_rx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[])
//
// DESCRIPTION
// i2c configured to receive
//--------------------------------------------------------------------------------------------------------------
void i2c_rx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[])
{
	i2c_acquire_bus(n);
	gpio_port_set_pin_state(10, 4, 1);
	i2c_tx_addr(n, addr, I2C_READ, delay_us);
	gpio_port_set_pin_state(10, 4, 0);
	//oct_nunchuk_tx_cmd ( 0x00, 0xF0);
	//oct_nunchuk_tx_cmd ( 0x00, 0x55);
	MCF_I2C_I2CR(n) &= 0xEF;
	MCF_I2C_I2CR(n) &= 0xF7;
	
	i2c_rx_byte(n, delay_us);
	
	for(int i = 1; i <= count; i++)
	{
		if(i == count -1)
		{
			MCF_I2C_I2CR(n) |= 0x08;
		}
		else if (i == count)
		{
			i2c_send_stop(n);
		}
		data[i-1] = i2c_rx_byte(n, delay_us);
		//gpio_port_set_pin_state(10, 4, 0);
	}
	i2c_reset(n);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: uint8 i2c_rx_byte(i2c_mod n, int delay_us)
//
// DESCRIPTION
// receives one byte
//--------------------------------------------------------------------------------------------------------------
uint8 i2c_rx_byte(i2c_mod n, int delay_us)
{
	uint8 rx_byte = MCF_I2C_I2DR(n);
	MCF_I2C_I2SR(n) &= 0xFD;
	
	dtim_busy_delay_us(dtim_0, delay_us);
	
	return rx_byte;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_send_stop(i2c_mod n)
//
// DESCRIPTION
// reconfigs tower to send stop bit
//--------------------------------------------------------------------------------------------------------------
void i2c_send_stop(i2c_mod n)
{
	MCF_I2C_I2CR(n) &= 0xDF;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_tx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[])
//
// DESCRIPTION
// transmits data
//--------------------------------------------------------------------------------------------------------------
void i2c_tx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[])
{
	i2c_acquire_bus(n);
	
	i2c_tx_addr(n,addr, I2C_WRITE, delay_us);
	
	for(int i = 0; i < count; i++)
	{
		//if(i ==0){gpio_port_set_pin_state(10, 4, 1);}
		
		i2c_tx_byte(n, data[i], delay_us);
		//gpio_port_set_pin_state(10, 4, 0);
	}
	
	i2c_send_stop(n);
	i2c_reset(n);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_tx_addr(i2c_mod n, uint8 addr, uint8 rw, int delay_us)
//
// DESCRIPTION
// transmits address
//--------------------------------------------------------------------------------------------------------------
void i2c_tx_addr(i2c_mod n, uint8 addr, uint8 rw, int delay_us)
{
	MCF_I2C_I2CR(n) |= 0x30;
	
	uint8 tx_byte = 0;
	
	tx_byte |= (addr << 1);
	tx_byte |= rw;
	
	i2c_tx_byte(n, tx_byte, delay_us);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void i2c_tx_byte(i2c_mod n, uint8 tx_byte, int delay_us)
//
// DESCRIPTION
// transmits one byte
//--------------------------------------------------------------------------------------------------------------
void i2c_tx_byte(i2c_mod n, uint8 tx_byte, int delay_us)
{
	int_inhibit_all();
	MCF_I2C_I2DR(n) = tx_byte;
	while(! i2c_tx_complete(n))
	{
		
	}
	

	MCF_I2C_I2SR(n) &= 0xFD;
	
	int_uninhibit_all();
	
	dtim_busy_delay_us(dtim_0,delay_us);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: uint8 i2c_tx_complete(i2c_mod n)
//
// DESCRIPTION
// returns a 1 when a transmit completes
//--------------------------------------------------------------------------------------------------------------
uint8 i2c_tx_complete(i2c_mod n)
{
	return (uint8)((MCF_I2C_I2SR(n) & 0x02) == 0x02);
}