//**************************************************************************************************************
// FILE: pit.c
//
// DECRIPTION
// contains functions to use the programmable interrupt timer
//
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "pit.h"

static int_isr g_pit0_callback = 0;
static int_isr g_pit1_callback = 0;


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pit0_init(int_isr p_pit0_callback)
//
// DESCRIPTION
// initializes pit0
//--------------------------------------------------------------------------------------------------------------
void pit0_init(int_isr p_pit0_callback)
{
	g_pit0_callback = p_pit0_callback;
	
	MCF_PIT0_PCSR &= 0xFE; //disables
	MCF_PIT0_PCSR |= (1<<5);
	MCF_PIT0_PCSR |= (1<<4);
	MCF_PIT0_PCSR |= (1<<3);
	MCF_PIT0_PCSR |= (1<<2);
	MCF_PIT0_PCSR |= (1<<1);
	MCF_PIT0_PCSR &= 0b1111000011111111;
	MCF_PIT0_PCSR |= (4 << 8); 
	
	MCF_PIT0_PMR = 4999; //set scaler
	
	int_config(55, 6, 3, pit0_isr); //sets interrupt
	
	//MCF_PIT0_PCSR |= 1; //enables
	
}

void pit0_enable()
{
	MCF_PIT0_PCSR |= 1;
}

void pit0_disable()
{
	MCF_PIT0_PCSR &= ~1;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void pit1_init(int_isr p_pit1_callback)
//
// DESCRIPTION
// initializes pit1
//--------------------------------------------------------------------------------------------------------------
void pit1_init(int_isr p_pit1_callback)
{
	g_pit1_callback = p_pit1_callback;
	
	MCF_PIT1_PCSR &= 0xFE; //disables
	MCF_PIT1_PCSR |= (1<<5);
	MCF_PIT1_PCSR |= (1<<4);
	MCF_PIT1_PCSR |= (1<<3);
	MCF_PIT1_PCSR |= (1<<2);
	MCF_PIT1_PCSR |= (1<<1);
	MCF_PIT1_PCSR &= 0b1111000011111111;
	MCF_PIT1_PCSR |= (8 << 8); 
	
	MCF_PIT1_PMR = 39062; //set scaler
	
	int_config(56, 2, 8, pit1_isr); //sets interrupt
	
	MCF_PIT1_PCSR |= 1; //enables
	
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION:__declspec(interrupt) void pit0_isr()
//
// DESCRIPTION
// pit0 isr calls callback function when interrupt is generated
//--------------------------------------------------------------------------------------------------------------
__declspec(interrupt) void pit0_isr()
{
	MCF_PIT0_PCSR |= (1<<2); // clear flag
	if(g_pit0_callback) g_pit0_callback();
	
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION:__declspec(interrupt) void pit1_isr()
//
// DESCRIPTION
// pit1 isr calls callback function when interrupt is generated
//--------------------------------------------------------------------------------------------------------------
__declspec(interrupt) void pit1_isr()
{
	MCF_PIT1_PCSR |= (1<<2); // clear flag
	if(g_pit1_callback) g_pit1_callback();
}