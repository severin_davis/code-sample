//**************************************************************************************************************
// FILE: main.c
//
// DECRIPTION
// initializes hardware and start ledoff
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************
#include "oct_ledm.h"
#include "dtim.h"
#include "uc_pushb.h"
#include "oct_nunchuk.h"
#include "i2c.h"
#include "ledoff.h"
#include "rng.h"

void hw_init();
void run();

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void main ()
//
// DESCRIPTION
// calls init and run
//--------------------------------------------------------------------------------------------------------------
void main ()
{
	hw_init();
	run();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void hw_init()
//
// DESCRIPTION
// inits all the hardware and stuff
//--------------------------------------------------------------------------------------------------------------
void hw_init()
{
	gpio_port_init(10, 4, 0, 1, 0);
	gpio_port_init(10, 6, 0, 1, 0);
	gpio_port_init(10, 7, 0, 1, 0);
	dtim_init(dtim_0);
	dtim_init(dtim_3);
	int_inhibit_all();
	uc_pushb_init(pushb1, pushb2);
	oct_nunchuk_init(i2c_mod_1);
	rng_init();
	oct_ledm_init();
	int_uninhibit_all();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void run()
//
// DESCRIPTION
// sets the led to initial state and then drops into loop
//--------------------------------------------------------------------------------------------------------------
void run()
{	
	pntr_reset();
	pattern_reset_all();
	set_rand();
	while(1)
	{}
}
