//**************************************************************************************************************
// FILE: oct_nunchuk.c
//
// DECRIPTION
// functions to utilize octopus nunchuk peripheral
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "oct_nunchuk.h"

i2c_mod g_i2c_mod;


#define OCT_NUNCHUK_REG_F0 0xF0
#define OCT_NUNCHUK_REG_FB 0xFB
#define OCT_NUNCHUK_REG_NONE 0x00
#define OCT_NUNCHUK_I2C_ADDR 0x52

uint8 g_pos_x;
uint8 g_pos_y;
uint8 g_button_c;
uint8 g_button_z;

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: vvoid oct_nunchuk_init(i2c_mod p_i2c_mod)
//
// DESCRIPTION
// initializes nunchuk
//--------------------------------------------------------------------------------------------------------------
void oct_nunchuk_init(i2c_mod p_i2c_mod)
{
	g_i2c_mod = p_i2c_mod;
	
	oct_nunchuk_reset();
	dtim_init(dtim_3);
	
	i2c_init(g_i2c_mod, 100, 8);
	
	oct_nunchuk_tx_cmd(OCT_NUNCHUK_REG_F0, 0x55);

	oct_nunchuk_tx_cmd(OCT_NUNCHUK_REG_FB, 0x00);
	
	pit1_init(oct_nunchuk_read);
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: initializes nunchuk
//
// DESCRIPTION
// reads current nunchuk state
//--------------------------------------------------------------------------------------------------------------
void oct_nunchuk_read()
{
	oct_nunchuk_tx_cmd ( OCT_NUNCHUK_REG_NONE, 0x00);
	
	uint8 data[6];

	i2c_rx(g_i2c_mod, OCT_NUNCHUK_I2C_ADDR, 6, 150, data);

	g_pos_x = data[0];
	g_pos_y = data[1];
	
	g_button_z = (uint8) !(data[5] & 1);
	
	g_button_c = (uint8) !((data[5] >> 1) & 1);
	
	if(g_button_z)
	{
		button_z();
	}
	
	if(g_button_c)
	{
		button_c();
	}

	if(g_pos_x < 75) pntr_right();
	if(g_pos_x > 180) pntr_left();
	
	if(g_pos_y < 75) pntr_down();
	if(g_pos_y > 180) pntr_up();
}


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_nunchuk_reset()
//
// DESCRIPTION
// unused
//--------------------------------------------------------------------------------------------------------------
void oct_nunchuk_reset()
{
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_nunchuk_tx_cmd(uint8 reg, uint8 cmd)
//
// DESCRIPTION
// transmits a command to the nunchuk over i2c
//--------------------------------------------------------------------------------------------------------------
void oct_nunchuk_tx_cmd(uint8 reg, uint8 cmd)
{
	uint8 tx_data[2];
	uint8 tx_count;
	if (reg!= OCT_NUNCHUK_REG_NONE)
	{
		tx_data[0]= reg;
		tx_data[1] = cmd;
		tx_count = 2;
	}
	else
	{
		tx_data[0] = cmd;
		tx_count = 1;
	}
	
	i2c_tx(g_i2c_mod, OCT_NUNCHUK_I2C_ADDR, tx_count, 150, tx_data);
	
	dtim_busy_delay_us(dtim_3,(300));
}