//**************************************************************************************************************
// FILE: gpio.c
//
// DECRIPTION
// contains functions to initialize and use gpio devices
// PROJECT: 3
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#include "gpio.h"

//base address definitions

#define PORT (0x40100000)
#define DDR (0x40100018)
#define SET (0x40100030)
#define CLR (0x40100048)
#define PAR (0x40100060)


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: gpio_port_get_pin_state(int, int)
//
// DESCRIPTION
// computes set address. Reads value of SET, shifts by pin number, and return value at LSB.
//--------------------------------------------------------------------------------------------------------------
int gpio_port_get_pin_state(int p_port, int p_pin)
{
	vuint8* newset = (vuint8*)(SET + p_port);
	
	int output = ((*newset) >> p_pin);
	output &= 1;
	
	return output;
}


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: gpio_port_init(int,int,int,int,int)
//
// DESCRIPTION
// Computes relevant register address and initializes to gpio function and sets data directions. Also sets state.
//--------------------------------------------------------------------------------------------------------------
void gpio_port_init(int p_port, int p_pin, int p_funct, int p_data_dir, int p_state) 
{
	//adds port offset to base address
	vuint8 * newddr = (vuint8*)(DDR+p_port);
	vuint8 * newset = (vuint8*)(SET+p_port);
	vuint8 * newclr = (vuint8*)(CLR+p_port);
	vuint8 * newpar = (vuint8*)(PAR+p_port);
			  
	if((p_port == 15)||(p_port == 14)||(p_port==17)||(p_port==18)||(p_port==19)||(p_port == 12)||(p_port == 13))
	{
		*newpar &= ~(3 << (2*p_pin)); //clears two bits to zero
		*newpar |= (p_funct << (2*p_pin)); //shifting pfunct over into cleared bits
	}
	else
	{
		*newpar &= ~(1 << (p_pin)); //clears one bit to zero
		*newpar |= (p_funct << (p_pin)); //shifting pfunct over into cleared bits
	}
	
	*newddr &= ~(1 << (p_pin)); // clears one bit
	*newddr |= (p_data_dir <<(p_pin)); //shifts data dir over to the cleared bit
	
	gpio_port_set_pin_state(p_port, p_pin, p_state);
}


//--------------------------------------------------------------------------------------------------------------
// FUNCTION: gpio_port_set_pin_state(int,int,int)
//
// DESCRIPTION
// sets specific pin of specific port to specific state
//--------------------------------------------------------------------------------------------------------------
void gpio_port_set_pin_state(int p_port, int p_pin, int p_state)
{
	vuint8 * newset = SET +(vuint8*)p_port;	 
	vuint8 * newclr = CLR +(vuint8*)p_port;
	
	if(p_state)	
	{
		*newset = (1 << (p_pin));
	}
	else
	{
		*newclr = (unsigned char)(~(1 << (p_pin)));
	}
}