//**************************************************************************************************************
// FILE: oct_ledm.c
//
// DECRIPTION
// functions to utilize octopus led matrix peripheral
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************
#include "oct_ledm.h"

uint8 *g_ledm_red; 
uint8 *g_ledm_green;
uint8 *g_ledm_blue;
uint8 g_row;

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_ledm_init()
//
// DESCRIPTION
// initializes pins to drive led matrix
//--------------------------------------------------------------------------------------------------------------
void oct_ledm_init()
{	
	gpio_port_init(4, 5, 0, 1, 0);
	gpio_port_init(4, 3, 0, 1, 1);
	
	qspi_init(8, 5000000, 0, 0);
	pit0_init(oct_ledm_refresh);
	
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_ledm_off()
//
// DESCRIPTION
// disables output to turn off matrix
//--------------------------------------------------------------------------------------------------------------
void oct_ledm_off()
{
	gpio_port_set_pin_state(4,3,1); //sets ledoe
	pit0_disable();
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_ledm_refresh()
//
// DESCRIPTION
// refreshes by calling refresh row and incrementing displayed row
//--------------------------------------------------------------------------------------------------------------
void oct_ledm_refresh()
{

	oct_ledm_refresh_row();
	g_row = (g_row + 1) % 8;
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_ledm_refresh_row()
//
// DESCRIPTION
// packs rgb values to display along with row to display and sends them to qspi to transmit to ledmatrix
//--------------------------------------------------------------------------------------------------------------
void oct_ledm_refresh_row()
{
	uint8 sdout[8];
	sdout[0] = g_ledm_green[g_row+8];
	sdout[1] = g_ledm_red[g_row+8];
	sdout[2] = g_ledm_blue[g_row+8]; 
	sdout[3] = (uint8) (~(1 << g_row));
	sdout[4] = g_ledm_green[g_row]; 
	sdout[5] = g_ledm_red[g_row];
	sdout[6] = g_ledm_blue[g_row];
	sdout[7] = sdout[3]; 
	
	gpio_port_set_pin_state(4,3,1); //sets ledoe
	gpio_port_set_pin_state(4, 5, 0);
	
	qspi_tx(8, sdout);
	
	gpio_port_set_pin_state(4, 5, 1);
	gpio_port_set_pin_state(4,3,0); 
	
}

//--------------------------------------------------------------------------------------------------------------
// FUNCTION: void oct_ledm_refresh_row()
//
// DESCRIPTION
// updates what hte matrix should display
//--------------------------------------------------------------------------------------------------------------
void oct_ledm_display_pattern(uint8 patternr[16], uint8 patterng[16], uint8 patternb[16])
{
	pit0_disable();
	g_ledm_red = patternr;
	g_ledm_green = patterng;
	g_ledm_blue = patternb;

	g_row = 0;
	
	pit0_enable();
}