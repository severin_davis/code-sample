//**************************************************************************************************************
// FILE: rng.h
//
// DECRIPTION
// header for rng.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef RNG_H
#define RNG_H

#include "support_common.h"

void rng_init();
uint32 rng_rand();

#endif