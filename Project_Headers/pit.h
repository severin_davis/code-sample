//**************************************************************************************************************
// FILE: pit.h
//
// DECRIPTION
// contains declarations for pit.c
// PROJECT: 3
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef PIT_H
#define PIT_H
#include "support_common.h"
#include "int.h"

void pit0_init(int_isr);
void pit1_init(int_isr);
void pit0_disable();
void pit0_enable();
void pit1_disable();
void pit1_enable();

__declspec(interrupt) void pit0_isr();
__declspec(interrupt) void pit1_isr();


#endif