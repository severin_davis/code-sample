//**************************************************************************************************************
// FILE: oct_ledm.h
//
// DECRIPTION
// header for oct_ledm.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef OCT_LEDM_H
#define OCT_LEDM_H

#include "pit.h"
#include "gpio.h"
#include "qspi.h"

void oct_ledm_init();
void oct_ledm_off() ;
void oct_ledm_refresh();
void oct_ledm_refresh_row();
void oct_ledm_display_pattern(uint8 patternr[16], uint8 patterng[16], uint8 patternb[16]);

#endif