//**************************************************************************************************************
// FILE: uc_led.h
//
// DECRIPTION
// contains declarations for uc_led.c
// PROJECT: 3
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef UC_LED_H

#define UC_LED_H
#include "support_common.h"

void uc_led_all_off();

void uc_led_all_on();

void uc_led_init();

void uc_led_off(int);

void uc_led_on(int);

void uc_led_toggle(int);

int uc_led_get_state(int);

#endif