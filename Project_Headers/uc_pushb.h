//**************************************************************************************************************
// FILE: uc_pushb.h
//
// DECRIPTION
// contains declarations for uc_pushb.c
// PROJECT: 3
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef UC_PUSHB_H
#define UC_PUSHB_H

#include "support_common.h"
#include "gpt.h"
#include "gpio.h"
#include "int.h"


void uc_pushb_init(int_isr p_pb1_callback, int_isr p_pb2_callback);

__declspec(interrupt) void uc_pushb1_isr();

__declspec(interrupt) void uc_pushb2_isr();

void uc_pushb_debounce(int button);

#endif