//**************************************************************************************************************
// FILE: qspi.h
//
// DECRIPTION
// header for qspi.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef QSPI_H
#define QSPI_H

#include "gpio.h"
#include "int.h"
#include "support_common.h"

void qspi_init(int bits, int freq, int cpol, int cpha);
void qspi_tx(int n, uint8 data[]);
void qspi_rx(int n, uint8 data[]);

#endif