//**************************************************************************************************************
// FILE: ledoff.h
//
// DECRIPTION
// header for ledoff.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef LEDOFF_H
#define LEDOFF_H
#include "support_common.h"
#include "oct_ledm.h"
#include "rng.h"

void pushb1();
void pushb2();
void pntr_reset();
void set_test();
void set_rand();
void set_win();
void button_c();
void button_z();
void pntr_up();
void pntr_down();
void pntr_right();
void pntr_left();
void pushb1();
void pushb2();
void pattern_reset(uint8 pattern[16]);
void pattern_reset_all();
void pattern_combine();
void toggle_adj();
void toggle(uint8 x, uint8 y);

#endif

