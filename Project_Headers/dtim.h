//***********************************************************************************************************************************
// FILE: dtim.h
//
// DECRIPTION:
// Contains functions for initializing DMA timers 0, 1, 2, and 3 and implementing a busy delay.
//
// AUTHORS:
// Kevin R. Burger (burgerk@asu.edu)
// Computer Science & Engineering
// Arizona State University
// Tempe, AZ 85287-8809
// http://www.devlang.com
//***********************************************************************************************************************************
#ifndef DTIM_H
#define DTIM_H

#include "support_common.h"
#include "int.h"

//===================================================================================================================================
// Global Type Definitions
//===================================================================================================================================

// Enumerated type to represent the DTIM channels 0, 1, 2, and 3.
typedef enum {
    dtim_0 = 0,
    dtim_1 = 1,
    dtim_2 = 2,
    dtim_3 = 3
} dtim;

//===================================================================================================================================
// Public Function Declarations
//===================================================================================================================================

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_busy_delay_ms()
//
// DESCRIPTION:
// The input parameter p_msecs is the number of milliseconds to delay. It is a 32-bit unsigned integer, so the range for p_msecs
// would normally be [1, 2^32 - 1]. Since there are 1000 usecs in 1 msec, this function passes p_msecs * 1000 to dtim_delay_us().
// But, in order to avoid overflow when multiplying p_msecs by 1000, we must ensure that p_msecs <= 4294967, so the real range for
// p_msecs is [1, 4294967]. If you pass in a value for p_msecs larger than 4,294,967 the timer delay function will not operate
// correctly, so don't do that.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_busy_delay_ms(dtim p_time, int p_msecs);

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_busy_delay_us()
//
// DESCRIPTION:
// The input parameter p_usecs is the number of microseconds to delay. It is a 32-bit unsigned integer, so the range for p_usecs is
// [1, 2^32 - 1]. The DMA timer is configured as an up-counter and counts in 1 us intervals. If p_usecs = 2^32 - 1 then the timer
// will count from 0, 1, 2, ..., 2^32. Now, 2^32 usecs = 2^32 / 10^6 secs = 4294.967296 secs = 4294.967296 / 60 mins = 71.582788267
// mins which is approximately 71:34:58 mins.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_busy_delay_us(dtim p_timer, int p_usecs);

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_disable()
//
// DESCRIPTION:
// Disables DMA timer p_timer.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_disable(dtim p_time);

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_init()
//
// DESCRIPTION:
// Initializes DMA timer p_timer so it will operate in reference match mode.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_init(dtim p_timer);

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_interrupt()
// 
// DESCRIPTION:
// Configures DMA timer p_timer to generate periodic interrupts with period p_usecs microseconds. If requested, the user's callback
// function p_callback will be called.
//-----------------------------------------------------------------------------------------------------------------------------------
void dtim_interrupt(dtim p_timer, int p_usecs, int_isr p_callback);

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_stopw_start()
//
// DESCRIPTION:
// Start DMA timer DTIM3 counting at 0. The frequency of the counter is F_SYS_MHZ.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(register_abi) void dtim_stopw_start();

//-----------------------------------------------------------------------------------------------------------------------------------
// FUNCTION: dtim_stopw_start()
//
// DESCRIPTION:
// Stops DMA timer DTIm3 from counting and returns the count. Since the frequency of the counter is F_SYS_MHZ the returned time will
// be PER_SYS_NS x returned_count.
//-----------------------------------------------------------------------------------------------------------------------------------
__declspec(register_abi) uint32 dtim_stopw_stop();

#endif
