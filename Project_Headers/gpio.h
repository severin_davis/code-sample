//**************************************************************************************************************
// FILE: gpio.h
//
// DECRIPTION
// contains declarations for gpio.c
// PROJECT: 3
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef GPIO_H

#define GPIO_H

#include "support_common.h"

int gpio_port_get_pin_state(int, int);

void gpio_port_init(int, int, int, int, int);

void gpio_port_set_pin_state(int, int, int);

#endif