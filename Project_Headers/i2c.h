//**************************************************************************************************************
// FILE: i2c.h
//
// DECRIPTION
// header for ledoff.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef I2C_H
#define I2C_H

#include "gpio.h"
#include "dtim.h"
#include "int.h"


typedef enum { i2c_mod_0 = 0, i2c_mod_1 = 1} i2c_mod;

void i2c_acquire_bus (i2c_mod n);
void i2c_init(i2c_mod n, int freq, uint8 addr);
void i2c_reset(i2c_mod n);
void i2c_rx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[]);
uint8 i2c_rx_byte(i2c_mod n, int delay_us);
void i2c_send_stop(i2c_mod n);
void i2c_tx(i2c_mod n, uint8 addr, int count, int delay_us, uint8 data[]);
void i2c_tx_addr(i2c_mod, uint8 addr, uint8 rw, int delay_us);
void i2c_tx_byte(i2c_mod n, uint8 tx_byte, int delay_us);
uint8 i2c_tx_complete(i2c_mod n);

#endif