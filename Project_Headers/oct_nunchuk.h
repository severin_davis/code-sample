//**************************************************************************************************************
// FILE: oct_nunchuk.h
//
// DECRIPTION
// header for oct_nunchuk.c
// PROJECT: 6/7
// AUTHORS
// Severin Davis (1203724478) (severin.davis@asu.edu)
// Cameron Keith (1204068627) (cameronk313@cox.net)
//
// COURSE: CSE325 Embedded Microprocessor Systems, Fall 2013
//**************************************************************************************************************

#ifndef OCT_NUNCHUK_H
#define OCT_NUNCHUK_H

#include "i2c.h"
#include "dtim.h"
#include "pit.h"
#include "ledoff.h"

void oct_nunchuk_init(i2c_mod p_i2c_mod);
void oct_nunchuk_read();
void oct_nunchuk_reset();
void oct_nunchuk_tx_cmd(uint8 reg, uint8 cmd);

#endif